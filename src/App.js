import React, { Component } from "react"
import { globalStyled , Button , Label } from './styled'
import PriorityQueue from "@raymond-lam/priority-queue"

const patients = [{ name: "yolo", code: 6 }, { name: "yoo", code: 5 }]

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      patient: {
        name: "",
        emergencyCode: 0
      },
      pq : new PriorityQueue([], (a, b) => {
        if (a.emergencyCode > b.emergencyCode) return -1
        else if (a.emergencyCode < b.emergencyCode) return 1
        else return 0 
      })
    }
  }

  

  resetForm = () => {
    this.setState({
      patient: {
        name: "",
        emergencyCode: 0
      }
    })
  }

  addQueue = e => {
    const { patient , pq } = this.state
    e.preventDefault()
    if(patient.name && patient.emergencyCode > 0){
      pq.enqueue(patient)
      this.resetForm()
    }
  }

  onInputChange = e => {
    this.setState({
      patient: {
        ...this.state.patient,
        name: e.target.value
      }
    })
  }

  nextQueue = e => {
    const { pq } = this.state
    e.preventDefault()
    pq.dequeue()
    this.forceUpdate()
  }

  clearQueue = e => {
    const { pq } = this.state
    e.preventDefault()
    pq.clear()
    this.forceUpdate()
  }

  onDropdownChange = e => {
    const value = parseInt(e.target.value, 10)
    this.setState({
      patient: {
        ...this.state.patient,
        emergencyCode: value
      }
    })
  }

  updatePatientsTable = e => {
    const { pq } = this.state
    e.preventDefault()
    pq.values()
  }
  

  render() {
    const { patient , pq } = this.state
    const mapPatientsTable = pq.map(i => 
      <tr style={{height:'50px'}}>
        <td>{i.name}</td>
        <td>{i.emergencyCode}</td>
      </tr>
    )
    return (
      <div style={{ 
          padding: "40px",
          display:'grid',
          gridGap:'20px',
          gridAutoColumns:'50%'
        }}>
        <h2 style={{
            textAlign:'center',
            gridColumn:'1/3',
            paddingBottom:'50px'
          }}>
            Patient Queuing System
        </h2>
        <div 
          style={{
            gridColumn:'1/2'
          }}>
          <form
            onSubmit={this.addQueue}
            style={{
              border: "1px solid",
              padding:'20px',
              borderRadius:'3px',
              marginRight:'100px'
            }}
          >
            <h4>Patient Form</h4>
            <div style={{
              display:'flex',
              justifyContent:'space-between',
              alignItems:'center'
            }}>
              <div>
                <input
                  style={{
                    height:'30px',
                    borderRadius:'3px',
                    paddingLeft:'10px',
                    borderStyle:'none',
                    border:'1px solid black',
                    width:'200px'
                  }}
                  placeholder="Name"
                  name="name"
                  type="text"
                  value={patient.name}
                  onChange={this.onInputChange}
                />
              </div>
              <div>
                <select
                  style={{
                    height:'35px',
                    width:'100px',
                    background:'white',
                    color:'grey',
                    borderStyle:'none',
                    border:'1px solid black',
                    paddingLeft:'5px',
                    borderRadius:'3px'
                  }}
                  value={patient.emergencyCode}
                  onChange={this.onDropdownChange}
                >
                  <option >Code</option>
                  <option value={1}>1</option>
                  <option value={2}>2</option>
                  <option value={3}>3</option>
                  <option value={4}>4</option>
                  <option value={5}>5</option>
                  <option value={6}>6</option>
                  <option value={7}>7</option>
                  <option value={8}>8</option>
                  <option value={9}>9</option>
                </select>
              </div>
              <div>
                <Button>
                  <strong>Add Queue</strong>
                </Button>
              </div>
            </div>
          </form>
          <div style={{marginTop:'30px',display:'flex'}}>
            <div style={{marginRight:'50px'}}>
            <Button
              onClick={this.nextQueue}
              ><strong>Next Queue</strong></Button>
            </div>
            <div>
            <Button
              onClick={this.clearQueue}
              ><strong>Clear Queue</strong></Button>
            </div>
          </div>
        </div>
        <div style={{
          gridColumn:'1/2',
          marginTop:'50px',
          display:'grid',
          gridGap:'20px'
        }}>
          <h3>Patient Who Being Treat Next</h3>
          <div><Label>Name : </Label> <strong>{pq.length > 0 && pq.peek().name}</strong></div>
          <br/>
          <div><Label>Code : </Label> <strong>{pq.length > 0 && pq.peek().emergencyCode}</strong></div>
        </div>
        <div style={{
          gridColumn:'2/3',
          gridRow:'4/2',
          display:'flex',
          flexDirection:'column',
          alignItems:'center'
        }}>
          <Label>Current Queue : <strong>{pq.length}</strong></Label>
          <table style={{
            border:'1px solid black',
            marginTop:'20px',
            borderRadius:'3px',
            width:'400px'
          }}>
          <thead style={{height:'50px'}}>
            <tr>
              <th>
                Patient Name
              </th>
              <th>
                Emergency Code
              </th>
            </tr>
          </thead>
          <tbody style={{textAlign:'center'}}>
           {mapPatientsTable}
          </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default App
