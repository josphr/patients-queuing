import styled , { injectGlobal } from 'styled-components'


injectGlobal`

  body {
    margin: 0;
    font-family: 'Roboto', sans-serif;
  }
`

export const Button = styled.button`
    width:150px;
    height:35px;
    background:white;
    color:black;
    border-radius:3px;
    border-style:none;
    border:1px solid black;
    transition:.3s;
    &:hover {
        background:black;
        color:white;
    }
`

export const Label = styled.label`
background:black;
padding:15px;
color:white;
border-radius:3px;
margin-right:20px;
`