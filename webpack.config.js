const webpack = require('webpack')
const path = require('path')

module.exports = {
    entry : path.resolve('src/index.js'),
    output : {
        filename : 'bundle.js',
        path : path.resolve(__dirname,'dist')
    },
    module : {
        rules : [
            {
                test : /\.jsx?$/,
                loader : 'babel-loader',
                exclude : /node_modules/,
                query : {
                    presets : ["es2015","react","stage-0"]
                }
            }
        ]
    },
    devServer : {
        contentBase: path.join(__dirname,'dist'),
        compress:true,
        port:8080
    }
}